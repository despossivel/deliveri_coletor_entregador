import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  button: {padding: 20},
  text: {color: '#000'},
});

export default styles;
