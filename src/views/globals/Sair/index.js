import React, {Component} from 'react';
import {TouchableOpacity, Text, ActivityIndicator} from 'react-native';
import {logOut} from '../../../services/auth';
import {withNavigation} from 'react-navigation';

import styles from './styles';

class Sair extends Component {
  constructor() {
    super();
    this.state = {
      load: false,
    };
  }
  async sair() {
    logOut();
    await this.setState({
      load: true,
    });
    this.props.navigation.navigate('Login');
  }

  render() {
    return (
      <TouchableOpacity onPress={e => this.sair()} style={styles.button}>
        {this.state.load ? (
          <ActivityIndicator size="large" color="#000" />
        ) : (
          <Text style={styles.text}>SAIR</Text>
        )}
      </TouchableOpacity>
    );
  }
}

export default withNavigation(Sair);
