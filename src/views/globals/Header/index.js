import {createStackNavigator, createAppContainer} from 'react-navigation';
import Main from '../Main';
import Pedido from '../../pages/Pedido';
import Mapa from '../../pages/Mapa';

const AppNavigator = createStackNavigator({
  Main,
  Pedido,
  Mapa,
});

export default createAppContainer(AppNavigator);
