import React from 'react';

import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

import Pendentes from '../../pages/Pendentes';
import Agendados from '../../pages/Agendados';
import AguardandoEntrega from '../../pages/AguardandoEntrega';

import Sair from '../Sair';

export default createMaterialTopTabNavigator(
  {
    Pendentes: Pendentes,
    Agendados: Agendados,
    'Aguardando Entrega': AguardandoEntrega,
  },
  {
    navigationOptions: {
      headerTitle: 'Deliveri',
      headerRight: <Sair />,
    },
    lazy: true,
    tabBarOptions: {
      pressColor: '#000',
      pressOpacity: 0.2,
      scrollEnabled: true,
      labelStyle: {
        fontSize: 15,
        fontWeight: 'bold',
      },
      activeTintColor: '#333',
      inactiveTintColor: '#333',
      style: {
        backgroundColor: '#fff',
      },
    },
  },
);
