import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
// import Geolocation from 'react-native-geolocation-service';
//import Geolocation from '@react-native-community/geolocation';

import styles from './styles';
import {signIn} from '../../../services/Requests';
import {session, isSession} from '../../../services/auth';
import {getItem} from '../../../models/AsyncStorageModel';

import logo from '../../../assets/logo_translucido_reversa.png';

// import OneSignal from 'react-native-onesignal';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      senha: '',
      load: false,
      dialog: false,
    };
    this.submit = this.submit.bind(this);

    // OneSignal.init("f045ff41-c5b9-4eb6-9c3d-9e1f0690c5bd");

    // OneSignal.addEventListener('received', this.onReceived());
    // OneSignal.addEventListener('opened', this.onOpened());
    // OneSignal.addEventListener('ids', this.onIds());
  }

  //   componentWillMount() {
  //     OneSignal.addEventListener('received', this.onReceived());
  //     OneSignal.addEventListener('opened', this.onOpened());
  //     OneSignal.addEventListener('ids', this.onIds());
  // }

  //   componentWillUnmount() {
  //     OneSignal.removeEventListener('received', this.onReceived());
  //     OneSignal.removeEventListener('opened', this.onOpened());
  //     OneSignal.removeEventListener('ids', this.onIds());
  //   }

  //   onReceived(notification) {
  //     if (notification) {
  //       console.log("Notification received: ", notification);
  //     }
  //   }

  //   onOpened(openResult) {
  //     if (openResult) {
  //       console.log('Message: ', openResult.notification.payload.body);
  //       console.log('Data: ', openResult.notification.payload.additionalData);
  //       console.log('isActive: ', openResult.notification.isAppInFocus);
  //       console.log('openResult: ', openResult);
  //     }
  //   }

  //   onIds(device) {
  //     console.log('Device info: ', device);
  //   }

  async componentDidMount() {
    let _isSession_ = JSON.parse(await isSession());
    if (_isSession_) {
      if (_isSession_.tipo === 3) {
        let PedidoEmCurso = JSON.parse(await getItem('Pedido'));

        // Geolocation.requestAuthorization();

        if (PedidoEmCurso && PedidoEmCurso.status === 4) {
          this.props.navigation.navigate('Pedido');
        }
      }

      this.props.navigation.navigate('Main');
    }
  }

  async submit() {
    this.setState({load: true});

    const dados = {email: this.state.email, senha: this.state.senha};
    const signUp = await signIn(dados);

    if (signUp.data.success) {
      session(signUp.data.validacao[0]);
      this.props.navigation.navigate('Main');
    } else {
      this.setState({load: false, dialog: signUp.data.validacao[0].msg});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={logo} />

        <TextInput
          style={styles.input}
          placeholder="Username"
          placeholderTextColor="#999"
          autoCapitalize="none"
          autoCorrect={false}
          underlineColorAndroid="transparent"
          onFocus={e => this.setState({dialog: false})}
          onChange={e => this.setState({dialog: false})}
          onChangeText={text => this.setState({email: text})}
        />

        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="#999"
          autoCapitalize="none"
          autoCorrect={false}
          underlineColorAndroid="transparent"
          secureTextEntry={true}
          onFocus={e => this.setState({dialog: false})}
          onChange={e => this.setState({dialog: false})}
          onChangeText={text => this.setState({senha: text})}
        />

        {this.state.dialog && (
          <View style={styles.dialog}>
            <Text style={styles.dialogText}>{this.state.dialog}</Text>
          </View>
        )}
        <TouchableOpacity onPress={this.submit} style={styles.button}>
          {this.state.load ? (
            <ActivityIndicator size="large" color="#FFF" />
          ) : (
            <Text style={styles.buttonText}>Login</Text>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
