import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingHorizontal: 30,
    backgroundColor: '#fff',
  },
  dialog: {
    backgroundColor: '#f00',
    padding: 8,
    marginTop: 10,
    borderRadius: 5,
  },
  dialogText: {
    color: '#FFF',
    textAlign: 'center',
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 25,
  },
  input: {
    height: 48,
    borderWidth: 1,
    borderColor: '#DDD',
    borderRadius: 4,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 10,
    backgroundColor: '#fff',
  },
  button: {
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333',
    borderRadius: 20,
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
});

export default styles;
