import React, {Component} from 'react';
import {View, Text, TouchableHighlight} from 'react-native';

import RecolherProduto from '../RecolherProduto';
import Coletado from '../Coletado';
import Parcial from '../Parcial';
import ColetarParaEntrega from '../ColetarParaEntrega';

import styles from '../styles';

export default class BodyModal extends Component {
  render() {
    return (
      <View style={styles.modalBody}>
        <View>
          <TouchableHighlight
            style={styles.closeModal}
            onPress={e => this.props.closeModal()}>
            <Text style={styles.textClose}>X</Text>
          </TouchableHighlight>
          {this.props.modalView === 'recolherProduto' && (
            <RecolherProduto
              {...this.props.recolherProduto}
              recolhido={this.props.recolhido}
            />
          )}
          {this.props.modalView === 'coletado' && (
            <Coletado
              load={this.props.load}
              concluirRecolhimento={this.props.concluirRecolhimento}
            />
          )}
          {this.props.modalView === 'parcial' && this.props.recolherProduto && (
            <Parcial
              quantidade={this.props.recolherProduto.quantidade}
              concluirParcialidade={this.props.concluirParcialidade}
            />
          )}

          {this.props.session &&
            this.props.session.tipo === 3 &&
            this.props.modalView === 'coletarParaEntrega' && (
              <ColetarParaEntrega
                {...this.props}
                confirmarColetaDeEntrega={this.props.confirmarColetaDeEntrega}
              />
            )}
        </View>
      </View>
    );
  }
}
