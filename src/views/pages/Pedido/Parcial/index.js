import React, {Component} from 'react';
// import {createStackNavigator, createAppContainer} from 'react-navigation';
import {
  View,
  Text,
  // BackHandler,
  TouchableOpacity,
  // TouchableHighlight,
  // Modal,
  // Alert,
} from 'react-native';

import styles from './styles';

export default class RecolherProduto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: this.props.quantidade,
    };
  }

  render() {
    return (
      <View>
        <Text style={styles.textCenter}>Selecione a quantidade em falta.</Text>
        <View style={styles.countParcial}>
          <TouchableOpacity
            style={styles.buttonCount}
            onPress={e =>
              this.setState.count <= this.props.quantidade &&
              this.setState({count: this.state.count + 1})
            }>
            <Text style={styles.text}>+</Text>
          </TouchableOpacity>
          <Text style={styles.valor}>{this.state.count}</Text>
          <TouchableOpacity
            style={styles.buttonCount}
            onPress={e =>
              this.state.count > 1
                ? this.setState({count: this.state.count - 1})
                : this.setState({count: this.state.count})
            }>
            <Text style={styles.text}>-</Text>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity
            style={styles.button}
            onPress={e => this.props.concluirParcialidade(this.state.count)}>
            <Text style={styles.textBranco}>Okey!</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
