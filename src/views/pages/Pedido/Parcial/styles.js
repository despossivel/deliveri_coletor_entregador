import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  countParcial: {
    // flex: 12,
    // top:10,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    // padding: 20,
    // backgroundColor:'#333',
  },
  buttonCount: {
    // backgroundColor: '#ccc',
    flex: 2,
  },
  valor: {
    textAlign: 'center',
    flex: 2,
    fontSize: 67,
    color: '#333',
    // backgroundColor: '#ccc'
  },
  textCenter: {
    textAlign: 'center',
    padding: 10,
    top: 10,
  },
  button: {
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    borderRadius: 20,
  },
  textBranco: {
    color: '#fff',
  },
  text: {
    color: '#000',
    textAlign: 'center',
    fontSize: 67,
  },
});

export default styles;
