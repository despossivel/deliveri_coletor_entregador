import React, {Component} from 'react';
// import {createStackNavigator, createAppContainer} from 'react-navigation';
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';

import styles from '../styles';

export default class ColetarParaEntrega extends Component {
  render() {
    return (
      <View>
        <View style={styles.bodyText}>
          <Text style={styles.textBodyText}>
            Deseja assumir a entrega deste pedido?
          </Text>
        </View>
        <View style={styles.modalBottomButtons}>
          <TouchableOpacity
            style={styles.buttonRecolhido}
            onPress={e => this.props.confirmarColetaDeEntrega(true)}>
            {this.props.load ? (
              <ActivityIndicator size="large" color="#FFF" />
            ) : (
              <Text style={styles.buttonText}>Sim</Text>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.buttonEmFalta}
            onPress={e => this.props.confirmarColetaDeEntrega(false)}>
            <Text style={styles.buttonText}>Não</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
