import React, {Component} from 'react';
// import {createStackNavigator, createAppContainer} from 'react-navigation';
import {
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  // TouchableHighlight,
  Modal,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {isSession} from '../../../services/auth';
import {getItem, setItem} from '../../../models/AsyncStorageModel';
import Produto from './Produto';
import NoLoad from '../../globals/NoLoad';

import Head from './Head';

// import RecolherProduto from './RecolherProduto';
// import Coletado from './Coletado';
// import Parcial from './Parcial';
// import ColetarParaEntrega from './ColetarParaEntrega';

import {
  finalizarColeta,
  assumirEntrega,
  iniciarColeta,
} from '../../../services/Requests';

import styles from './styles';
import BodyModal from './BodyModal';

export default class Pedido extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this._didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  static navigationOptions = {
    headerTitleStyle: {alignSelf: 'center', fontWeight: 'bold'},
    title: 'Pedido',
    textAlign: 'center',
    load: false,
    headerStyle: {
      backgroundColor: '#000',
    },
    headerTintColor: '#fff',
  };

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
    let pedido = JSON.parse(await getItem('Pedido'));
    let _session_ = JSON.parse(await isSession());

    this.setState({pedido: {...pedido}, session: {..._session_}});
  }

  onBackButtonPressAndroid = () => this.props.navigation.navigate('Main');

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();

    this.setState({
      modalVisible: false,
    });
  }

  iniciarColeta = async () => {
    this.setState({
      load: true,
    });

    let pedido = this.state.pedido;
    pedido.token = this.state.session.token;
    pedido._idColetor = this.state.session._id;

    let response = await iniciarColeta(pedido);

    if (response.data.success) {
      pedido.status = 2;

      this.setState({
        pedido,
        load: false,
      });
    } else {
      Alert.alert(
        ':(',
        response.data.validacao[0].msg,
        [{text: 'OK', onPress: () => this.closeModal()}],
        {cancelable: false},
      );
      this.setState({
        load: false,
      });
    }
  };

  recolherProduto = produto => {
    if (this.state.pedido.status === 2) {
      this.setState({
        recolherProduto: produto,
        modalVisible: true,
        modalView: 'recolherProduto',
      });
    }
  };

  recolhido = (produto, situacao) => {
    let produtos = this.state.pedido.produtos;
    let produtoSelecionado = this.state.recolherProduto;
    let indice;
    let modalView = false;
    let produtoNaLista = produtos.find((produto_, i) => {
      if (produto_._id === produto._id) {
        indice = i;
        delete produtos[i];
        return produto_._id === produto._id;
      }
      return;
    });

    produtoNaLista.situacao = situacao;
    produtoSelecionado.situacao = situacao;
    produtos[indice] = produtoNaLista;

    let pedido = this.state.pedido;
    pedido.produtos = produtos;

    if (situacao === 'parcial') {
      modalView = situacao;
    }

    this.setState({
      pedido,
      produtoSelecionado,
      modalView,
      modalVisible: false,
    });
  };

  concluirParcialidade = quantidade => {
    let produtoSelecionado = this.state.recolherProduto;
    produtoSelecionado.quantidadeParcial = quantidade;
    let produtos = this.state.pedido.produtos;
    let indice;

    produtos.find((produto, i) => {
      if (produto._id === produtoSelecionado._id) {
        indice = i;
        return i;
      }
      return;
    });

    delete produtos[indice];
    produtos[indice] = produtoSelecionado;

    let pedido = this.state.pedido;
    pedido.produtos = produtos;

    this.setState({
      pedido,
      modalView: false,
      modalVisible: false,
    });
  };

  concluirRecolhimento = async situacao => {
    this.setState({
      load: true,
    });

    if (situacao) {
      let produtos = this.state.pedido.produtos;
      let check = produtos.findIndex(produto => produto.situacao === undefined);

      if (check === -1) {
        let pedido = this.state.pedido;
        pedido.token = this.state.session.token;

        let response = await finalizarColeta(pedido).catch(e => console.log(e));
        if (response.data.success) {
          this.setState({
            load: false,
          });

          this.props.navigation.state.params.onNavigateBack();
          this.props.navigation.navigate('Main');
        } else {
          Alert.alert(
            ':(',
            response.data.validacao[0].msg,
            [{text: 'OK', onPress: () => this.closeModal()}],
            {cancelable: false},
          );
          this.setState({
            load: false,
          });
        }
      } else {
        Alert.alert(
          'Atenção Shopper',
          'Colete todos os produtos da lista ou sinalize',
          [
            {
              text: 'OK',
              onPress: () => this.closeModal(),
            },
          ],
          {cancelable: false},
        );

        this.setState({
          load: false,
        });

        this.closeModal();
      }
    } else {
      this.setState({
        load: false,
      });
      this.closeModal();
    }
  };

  confirmarColetaDeEntrega = async status => {
    this.setState({
      load: true,
    });

    if (status) {
      let dados = {
        token: this.state.session.token,
        _idEntregador: this.state.session._id,
        _id: this.state.pedido._id,
      };

      let response = await assumirEntrega(dados);

      if (response.data.success) {
        let pedido = this.state.pedido;
        pedido.status = 4;

        await setItem('Pedido', JSON.stringify(pedido));

        this.setState({
          pedido,
          load: false,
        });
      } else {
        response.data.validacao.map(msg =>
          Alert.alert(
            'Ops',
            msg,
            [{text: 'OK', onPress: () => this.closeModal()}],
            {cancelable: false},
          ),
        );

        this.setState({
          load: false,
        });
      }

      this.closeModal();
    } else {
      this.closeModal();
      this.setState({
        // pedido,
        load: false,
      });
    }
  };

  closeModal = () =>
    this.setState({
      modalVisible: false,
      modalView: false,
    });

  //// FORMAS DE PAGAMENTO
  //1 = Pagar em dinheiro
  //2 = Cartão na entrega
  //3 = Cartão online
  formaDePagamento = () => {
    switch (this.state.pedido.formaDePagamento._id) {
      case 1:
        return 'Receber pagamento em dinheiro';
        break;

      case 2:
        return 'Receber no cartão - Leva maquina';
        break;

      case 3:
        return 'Pago ONLINE';
        break;
    }
  };

  formatReal = int => {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
    if (tmp.length > 6) {
      tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');
    }

    return tmp;
  };
  getMoney = str => {
    str = String(str);
    return parseInt(str.replace(/[\D]+/g, ''));
  };

  total = () =>
    this.state.pedido.produtos.reduce(
      (acc, item) => acc + item.quantidade * this.getMoney(item.valor),
      0,
    );

  render() {
    //status pedido
    //0 em aberto
    //1 em analise
    //2 em andamento
    //3 aguardadno entrega
    //4 saiu para entrega
    //5 agendado
    //6 entregue

    //// FORMAS DE PAGAMENTO
    //1 = Pagar em dinheiro
    //2 = Cartão na entrega
    //3 = Cartão online

    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          presentationStyle="overFullScreen"
          transparent={true}
          visible={this.state.modalVisible}>
          <BodyModal
            {...this.state}
            recolhido={this.recolhido.bind(this)}
            concluirRecolhimento={this.concluirRecolhimento.bind(this)}
            concluirParcialidade={this.concluirParcialidade.bind(this)}
            confirmarColetaDeEntrega={this.confirmarColetaDeEntrega.bind(this)}
            closeModal={this.closeModal.bind(this)}
          />
        </Modal>

        {this.state.pedido ? (
          <View>
            <Head {...this.state.pedido} />

            <View style={styles.bodyProdutos}>
              {this.state.pedido.produtos.map(produto => (
                <Produto
                  key={produto._id}
                  {...produto}
                  recolherProduto={this.recolherProduto.bind(this)}
                />
              ))}
            </View>

            <View style={styles.flex3}>
              {this.state.session.tipo === 3 && (
                <View style={styles.tagPagamento}>
                  <Text style={styles.textPagamentoValor}>
                    R$ {this.formatReal(this.total())}{' '}
                  </Text>
                  <Text style={styles.textPagamento}>
                    {this.formaDePagamento()}
                  </Text>
                </View>
              )}

              {this.state.pedido.endereco ? (
                <Text style={styles.endereco}>
                  {this.state.pedido.endereco.rua},{' '}
                  {this.state.pedido.endereco.numero} -{' '}
                  {this.state.pedido.endereco.bairro} -{' '}
                  {this.state.pedido.endereco.cidade} -{' '}
                  {this.state.pedido.endereco.estado}
                </Text>
              ) : (
                'Endereço não encontrado, entre em contato com o cliente pelo telefone'
              )}
            </View>

            {/** COLETA DO COLETOR */}
            {this.state.session.tipo === 2 && this.state.pedido.status === 2 && (
              <View style={styles.bodyButtons}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() =>
                    this.setState({modalVisible: true, modalView: 'coletado'})
                  }>
                  <Text style={styles.buttonText}>Coletado</Text>
                </TouchableOpacity>
              </View>
            )}

            {this.state.session.tipo === 2 && this.state.pedido.status < 2 && (
              <View style={styles.bodyButtons}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.iniciarColeta()}>
                  {this.state.load ? (
                    <ActivityIndicator size="large" color="#FFF" />
                  ) : (
                    <Text style={styles.buttonText}>Iniciar coleta</Text>
                  )}
                </TouchableOpacity>
              </View>
            )}
            {/** #################################################################### */}

            {/** COLETA DO ENTREGADOR */}
            {this.state.session.tipo === 3 && this.state.pedido.status === 3 && (
              <View style={styles.bodyButtons}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() =>
                    this.setState({
                      modalVisible: true,
                      modalView: 'coletarParaEntrega',
                    })
                  }>
                  <Text style={styles.buttonText}>Coletar para entrega</Text>
                </TouchableOpacity>
              </View>
            )}

            {this.state.session.tipo === 3 && this.state.pedido.status === 4 && (
              <View style={styles.bodyButtons}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.props.navigation.navigate('Mapa')}>
                  <Text style={styles.buttonText}>Traçar rota</Text>
                </TouchableOpacity>
              </View>
            )}

            {/** ####################################################################### */}
          </View>
        ) : (
          <NoLoad />
        )}
      </View>
    );
  }
}
