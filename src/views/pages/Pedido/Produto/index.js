import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight} from 'react-native';
import {IP, PORT} from '../../../../services/IP';
import styles from './styles';

export default class Produto extends Component {
  selecionar = produto => this.props.recolherProduto(produto);

  situacao = () => {
    let style;

    switch (this.props.situacao) {
      case 'falta':
        style = styles.bodyFalta;
        break;
      case 'parcial':
        style = styles.bodyParcial;
        break;
      case 'recolhido':
        style = styles.bodyRecolhido;
        break;

      default:
        style = styles.body;
        break;
    }
    return style;
  };

  render() {
    return (
      <TouchableHighlight
        key={this.props._id}
        style={styles.item}
        onPress={e => this.selecionar(this.props)}>
        <View style={this.situacao()}>
          <Image
            style={styles.foto}
            source={{uri: `http://${IP}:${PORT}/uploads/${this.props.foto}`}}
          />
          <View>
            {/* <Text>{this.props.categoria}</Text> */}
            <Text style={styles.nome}>
              <Text style={styles.quantidade}>{this.props.quantidade}x</Text> -{' '}
              {this.props.nome}{' '}
            </Text>
            <Text style={styles.descricao}>{this.props.descricao}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
