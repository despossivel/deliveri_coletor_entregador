import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  item: {
    padding: 4,
    height: 80,
    flex: 3,
    flexDirection: 'column',
    minHeight: 80,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    marginBottom: 8,
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 60,
  },
  foto: {
    width: 50,
    height: 50,
  },
  body: {
    flex: 3,
    flexDirection: 'row',
    padding: 10,
  },
  bodyFalta: {
    flex: 3,
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#f00',
  },
  bodyParcial: {
    flex: 3,
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#ffc107',
  },
  bodyRecolhido: {
    flex: 3,
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#21c59f',
  },
  nome: {
    marginLeft: 15,
    width: '80%',
    fontWeight: '900',
  },
  descricao: {
    width: '80%',
    marginLeft: 15,
    height: 40,
    maxHeight: 40,
  },
  quantidade: {
    fontSize: 20,
  },
});

export default styles;
