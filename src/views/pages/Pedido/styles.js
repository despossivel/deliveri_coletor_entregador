import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f5f3f4',
    overflow: 'scroll',
  },
  head: {
    padding: 8,
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#e2e1e2',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    height: 80,
    minHeight: 80,
  },
  user: {
    fontWeight: '900',
    width: '80%',
  },
  tag: {
    width: '20%',
    height: 25,
    backgroundColor: '#ccc',
    borderRadius: 7,
    justifyContent: 'center',
  },

  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  emAberto: {
    width: '20%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#ff9800',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  emAnalise: {
    width: '20%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#ff9800',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  emAndamento: {
    right: 25,
    width: '27%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#ff9800',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },

  aguardandoEntrega: {
    right: 80,
    width: '40%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#3f51b5',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  saiuParaEntrega: {
    width: '40%',
    right: 80,
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#8d299e',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  agendado: {
    width: '20%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#2196f3',
  },
  entregue: {
    width: '20%',
    height: 25,
    borderRadius: 7,
    justifyContent: 'center',
    backgroundColor: '#2196f3',
  },
  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////

  textCenter: {
    textAlign: 'center',
  },
  foto: {
    alignSelf: 'center',
    width: 100,
    height: 100,
    borderRadius: 60,
  },
  nome: {
    fontSize: 17,
    fontWeight: '900',
  },
  endereco: {
    fontWeight: 'bold',
    padding: 10,
    paddingBottom: 30,
    backgroundColor: '#fff',
    textAlign: 'center',
  },
  bodyButtons: {
    // backgroundColor:'#f00',
    padding: 20,
    backgroundColor: '#fff',
  },
  button: {
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    // marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    borderRadius: 20,
  },
  modalBottomButtons: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  buttonEmFalta: {
    flex: 2,
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f44336',
    borderRadius: 20,
    marginLeft: 4,
    marginRight: 4,
  },
  buttonParcial: {
    flex: 2,
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffc107',
    borderRadius: 20,
    marginLeft: 4,
    marginRight: 4,
  },
  buttonRecolhido: {
    flex: 2,
    height: 48,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4caf50',
    borderRadius: 20,
    marginLeft: 4,
    marginRight: 4,
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  modalBody: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    width: '100%',
    height: 170,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 5,
  },
  textClose: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '900',
    color: '#333',
  },
  bodyText: {
    padding: 10,
    top: 30,
  },
  closeModal: {
    justifyContent: 'center',
    top: 8,
    position: 'absolute',
    backgroundColor: '#f5f3f4',
    borderRadius: 50,
    width: 25,
    height: 25,
    right: 15,
    zIndex: 9,
  },
  textBodyText: {
    textAlign: 'center',
    fontSize: 17,
  },
  tagPagamento: {
    padding: 10,
    backgroundColor: '#fff',
    paddingBottom: 25,
    // marginBottom: 10
  },
  textPagamento: {
    fontSize: 17,
    textAlign: 'center',
  },
  textPagamentoValor: {
    fontSize: 22,
    textAlign: 'center',
    fontWeight: '900',
  },
  bodyProdutos: {
    flex: 2,
    padding: 15,
    backgroundColor: '#fff',
  },
  flex3: {flex: 3},
});

export default styles;
