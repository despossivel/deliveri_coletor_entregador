import React, {Component} from 'react';
import {View, Text, ActivityIndicator, TouchableOpacity} from 'react-native';

import styles from '../styles';

export default class RecolherProduto extends Component {
  render() {
    return (
      <View>
        <View style={styles.bodyText}>
          <Text>
            Todos os produtos foram coletados? ou sinalizados de acordo com sua
            situação atual em estoque?
          </Text>
        </View>
        <View style={styles.modalBottomButtons}>
          <TouchableOpacity
            style={styles.buttonRecolhido}
            onPress={e => this.props.concluirRecolhimento(true)}>
            {this.props.load ? (
              <ActivityIndicator size="large" color="#FFF" />
            ) : (
              <Text style={styles.buttonText}>SIM</Text>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.buttonEmFalta}
            onPress={e => this.props.concluirRecolhimento(false)}>
            <Text style={styles.buttonText}>Não</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
