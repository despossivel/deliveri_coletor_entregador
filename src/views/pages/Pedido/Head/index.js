import React, {Component} from 'react';
import {View, Text} from 'react-native';
import styles from '../styles';

export default class Head extends Component {
  statusStyle = status => {
    let style;
    switch (status) {
      case 0:
        style = styles.emAberto;
        break;
      case 1:
        style = styles.emAnalise;
        break;
      case 2:
        style = styles.emAndamento;
        break;
      case 3:
        style = styles.aguardandoEntrega;
        break;
      case 4:
        style = styles.saiuParaEntrega;
        break;
      case 5:
        style = styles.agendado;
        break;

      case 6:
        style = styles.entregue;
        break;
    }
    return style;
  };

  status = status => {
    let text;
    switch (status) {
      case 0:
        text = 'Em aberto';
        break;
      case 1:
        text = 'Em analise';
        break;
      case 2:
        text = 'Em andamento';
        break;
      case 3:
        text = 'Aguardando entrega';
        break;
      case 4:
        text = 'Saiu para entrega';
        break;
      case 5:
        text = 'Agendado';
        break;
      case 6:
        text = 'Entregue';
        break;
    }
    return text;
  };

  render() {
    return (
      <View style={styles.head}>
        <View style={styles.user}>
          <Text style={styles.nome}> {this.props.usuario[0].nome}</Text>
          <Text>{this.props.usuario[0].telefone}</Text>
          <Text>{this.props.dataHoraBR}</Text>

          {this.props.agendamento && (
            <Text>Agendado para: {this.props.dataHoraAgendamentoBR}</Text>
          )}
        </View>
        <View style={this.statusStyle(this.props.status)}>
          <Text style={styles.textCenter}>
            {this.status(this.props.status)}
          </Text>
        </View>
      </View>
    );
  }
}
