import React, {Component} from 'react';
// import {createStackNavigator, createAppContainer} from 'react-navigation';
import {
  View,
  Text,
  // BackHandler,
  TouchableOpacity,
  // TouchableHighlight,
  // Modal,
  // Alert,
} from 'react-native';

import styles from '../styles';

export default class RecolherProduto extends Component {
  render() {
    return (
      <View>
        <View style={styles.bodyText}>
          <Text style={styles.textBodyText}>
            Qual a situação atuao do produto {this.props.nome} em estoque?
          </Text>
        </View>

        <View style={styles.modalBottomButtons}>
          <TouchableOpacity
            style={styles.buttonEmFalta}
            onPress={e => this.props.recolhido(this.props, 'falta')}>
            <Text style={styles.buttonText}>Em falta</Text>
          </TouchableOpacity>

          {this.props.quantidade > 1 && (
            <TouchableOpacity
              style={styles.buttonParcial}
              onPress={e => this.props.recolhido(this.props, 'parcial')}>
              <Text style={styles.buttonText}>Parcial</Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            style={styles.buttonRecolhido}
            onPress={e => this.props.recolhido(this.props, 'recolhido')}>
            <Text style={styles.buttonText}>Recolhido</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
