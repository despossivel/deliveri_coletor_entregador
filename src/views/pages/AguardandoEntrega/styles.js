import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 8,
    backgroundColor: '#f5f3f4',
  },
  item: {
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#4caf50',
    height: 100,
    borderRadius: 5,
    marginBottom: 5,
  },
  input: {
    height: 48,
    borderWidth: 1,
    borderColor: '#1b5f1d',
    borderRadius: 4,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 30,
  },
  button: {
    height: 48,
    borderRadius: 4,
    fontSize: 16,
    paddingHorizontal: 20,
    marginTop: 10,
    backgroundColor: '#666',
    justifyContent: 'center',
    alignItems: 'center',
  },

  //////////////////////////////////////////
  ////////////////////////////////////////////
  head: {
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#1b5f1d',
    borderBottomWidth: 2,
    padding: 10,
    height: 30,
    minHeight: 25,
  },
  idPedido: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000',
    width: '70%',
    // height: 30,
    paddingTop: 6,
  },
  totalProdutos: {
    textAlign: 'right',
    color: '#000',
    width: '30%',
    // height: 30,
    paddingTop: 6,
  },
  ////////////////////////////////////////////
  ////////////////////////////////////////////
  body: {
    flex: 2,
    flexDirection: 'row',
    // backgroundColor:'#252424'
  },
  estabelecimento: {
    padding: 8,
    flex: 1,
    flexDirection: 'row',
    // backgroundColor:'#f00',
    width: '25%',
  },
  logo: {
    width: 25,
    height: 25,
    borderRadius: 60,
    backgroundColor: '#fff',
  },
  textItem: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    marginLeft: 8,
    marginTop: 4,
    fontWeight: 'bold',
    fontSize: 12,
    color: '#000',
  },
  dataHora: {
    paddingRight: 8,
    marginTop: 6,
  },
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  buttonText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  nenhumPedido: {
    padding: 10,
    backgroundColor: '#ccc',
    textAlign: 'center',
  },
  textCenter: {
    textAlign: 'center',
  },
});

export default styles;
