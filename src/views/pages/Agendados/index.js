import React, {Component} from 'react';
import {
  Text,
  ScrollView,
  TouchableOpacity,
  View,
  Image,
  RefreshControl,
} from 'react-native';
import {agendados} from '../../../services/Requests';
import {isSession} from '../../../services/auth';
import {setItem} from '../../../models/AsyncStorageModel';

import styles from './styles';

export default class Agendados extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.redirectProduto = this.redirectProduto.bind(this);
  }

  getPedidos = async () => {
    await this.setState({
      refresh: true,
    });

    let _isSession_ = JSON.parse(await isSession());
    let json = {token: _isSession_.token, tipoUsuario: _isSession_.tipo};
    if (_isSession_.tipo === 2) {
      json._idEstabelecimento = _isSession_._idEstabelecimento;
    }

    let pedidos = await agendados(json);

    if (!pedidos.data.errors) {
      this.setState({
        pedidos: pedidos.data,
      });
    } else {
      this.setState({
        pedidos: false,
      });
    }

    await this.setState({
      refresh: false,
    });
  };

  _onRefresh = async () => await this.getPedidos();

  handleOnNavigateBack = async () => await this._onRefresh();

  componentDidMount = async () => await this._onRefresh();

  redirectProduto = async pedido => {
    await setItem('Pedido', JSON.stringify(pedido));
    this.props.navigation.navigate('Pedido');
  };

  pedido = () => {
    if (this.state.pedidos) {
      let pedidos = this.state.pedidos.map(pedido => (
        <ScrollView key={pedido._id} contentContainerStyle={styles.container}>
          <TouchableOpacity
            onPress={e => this.redirectProduto(pedido)}
            style={styles.item}>
            <View style={styles.head}>
              <Text style={styles.idPedido}>#{pedido._id}</Text>
              <Text style={styles.totalProdutos}>
                {pedido.produtos.length} produtos
              </Text>
              {/* <Text style={styles.textItem}>{pedido.dataHoraBR}</Text> */}
            </View>
            <View style={styles.body}>
              {pedido.produtos.map(produto => (
                <View key={produto._id} style={styles.estabelecimento}>
                  {/* <Image style={styles.logo} source={{ uri: `http://192.168.3.19:5000/uploads/${produto.estabelecimento.logo}` }} /> */}
                  <Image
                    style={styles.logo}
                    source={{
                      uri: `https://deliveri.com.br/static/uploads/${
                        produto.estabelecimento.logo
                      }`,
                    }}
                  />
                  <Text style={styles.textItem}>
                    {produto.estabelecimento.nome}
                  </Text>
                </View>
              ))}

              <View style={styles.dataHora}>
                <Text style={styles.textItem}>{pedido.dataHoraBR}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      ));

      return pedidos;
    } else {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.nenhumPedido}>
            <Text style={styles.textCenter}>Nenhum pedido!</Text>
          </View>
        </ScrollView>
      );
    }
  };

  render() {
    return (
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refresh}
            onRefresh={this._onRefresh}
          />
        }>
        {this.pedido()}
      </ScrollView>
    );
  }
}
