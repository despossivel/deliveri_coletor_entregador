import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';

import styles from '../styles';

export default class Buttons extends Component {
  render() {
    let {location, load} = this.props;

    return (
      <View style={styles.flex}>
        {location ? (
          <View style={styles.modalBottomButtons}>
            <TouchableOpacity
              style={styles.buttonEntregue}
              onPress={e => this.props.finalizar(true)}>
              {load ? (
                <ActivityIndicator size="large" color="#FFF" />
              ) : (
                <Text style={styles.buttonText}>Entregue!!!</Text>
              )}
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.modalBottomButtons}>
            <TouchableOpacity
              style={styles.buttonEntregue}
              onPress={e => this.props.getLocationUpdates()}>
              <Text style={styles.buttonText}>Obter minhas coordenadas</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}
