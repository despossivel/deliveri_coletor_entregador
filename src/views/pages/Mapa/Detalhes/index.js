import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from '../styles';

export default class Detalhes extends Component {
  constructor(props) {
    super(props);
  }

  formatReal = int => {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
    if (tmp.length > 6) {
      tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');
    }

    return tmp;
  };
  getMoney = str => {
    str = String(str);
    return parseInt(str.replace(/[\D]+/g, ''));
  };

  formaDePagamento = formaDePagamento => {
    switch (formaDePagamento._id) {
      case 1:
        return 'Receber pagamento em dinheiro';
        break;

      case 2:
        return 'Receber no cartão - Leva maquina';
        break;

      case 3:
        return 'Pago ONLINE';
        break;
    }
  };

  total = produtos =>
    produtos.reduce(
      (acc, item) => acc + item.quantidade * this.getMoney(item.valor),
      0,
    );

  render() {
    let pedido = this.props;
    let {usuario, endereco, produtos, formaDePagamento, infoRota} = pedido;

    return (
      <View style={styles.flex1}>
        <Text style={styles.fontBolder}>{usuario[0].nome}</Text>
        <Text>{usuario[0].telefone}</Text>
        <Text style={styles.fontBolder}>
          {pedido.endereco.rua}, {endereco.numero} - {endereco.bairro} -{' '}
          {endereco.cidade} - {endereco.estado}
        </Text>

        {infoRota && (
          <View>
            <Text>{infoRota.distancia}</Text>
            <Text>{infoRota.duracao}</Text>
          </View>
        )}

        <View style={styles.tagPagamento}>
          <Text style={styles.textPagamentoValor}>
            R$ {this.formatReal(this.total(produtos))}{' '}
          </Text>
          <Text style={styles.textPagamento}>
            {this.formaDePagamento(formaDePagamento)}
          </Text>
        </View>
      </View>
    );
  }
}
