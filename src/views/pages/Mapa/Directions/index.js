import React, {Component} from 'react';
// import MapViewDirections from 'react-native-maps-directions';
// import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

// "react-native-maps": "^0.25.0",
// "react-native-maps-directions": "^1.7.0",

import styles from '../styles';

export default class Directions extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {location, destination} = this.props;

    return (
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        // minZoomLevel={18}
        showsPointsOfInterest={false}
        showsBuildings={false}>
        <Marker
          coordinate={{
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          }}
        />

        <MapViewDirections
          origin={{
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          }}
          destination={{latitude: destination.lat, longitude: destination.lng}}
          apikey={'AIzaSyATRZCp1G_W1IcKrad8nofGcbZ_s6Yyr04'}
          strokeWidth={7}
          strokeColor="#333"
          optimizeWaypoints={true}
          onReady={result =>
            this.setState({
              infoRota: {
                distancia: `Distância ${result.distance} km`,
                duracao: `Duração estimada ${result.duration.toFixed(2)} min.`,
              },
            })
          }
          onError={errorMessage => {}}
        />
      </MapView>
    );
  }
}
