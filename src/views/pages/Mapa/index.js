import React, {Component} from 'react';

import {View, Alert} from 'react-native';

import {getItem} from '../../../models/AsyncStorageModel';
import styles from './styles';
 

import {isSession} from '../../../services/auth';
import {finalizarEntrega} from '../../../services/Requests';

import Detalhes from './Detalhes';
import Buttons from './Buttons';
// import Directions from './Directions';

//    "react-native-geolocation-service": "^3.1.0",

export default class RecolherProduto extends Component {
  state = {
    loading: false,
    updatesEnabled: false,
    location: false,
  };
  watchId = null;

  // componentWillUnmount = () => this.removeLocationUpdates();

  // hasLocationPermission = async () => {
  //     if (Platform.OS === 'ios' ||
  //         (Platform.OS === 'android' && Platform.Version < 23)) {
  //         return true;
  //     }
  //     const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  //     if (hasPermission) return true;
  //     const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  //     if (status === PermissionsAndroid.RESULTS.GRANTED) return true;
  //     if (status === PermissionsAndroid.RESULTS.DENIED) {
  //         ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
  //     } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
  //         ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
  //     }

  //     return false;
  // }

  // getLocation = async () => {
  //     const hasLocationPermission = await this.hasLocationPermission();
  //     if (!hasLocationPermission) return;
  //     this.setState({ loading: true }, () => Geolocation.getCurrentPosition(
  //         (position) => this.setState({ location: position, loading: false }),
  //         (error) => this.setState({ location: error, loading: false }),
  //         { enableHighAccuracy: false, timeout: 15000, maximumAge: 1000, distanceFilter: 50, forceRequestLocation: true }
  //     ));
  // }

  // getLocationUpdates = async () => {
  //     const hasLocationPermission = await this.hasLocationPermission();
  //     if (!hasLocationPermission) return;
  //     this.setState({ updatesEnabled: true }, () => this.watchId = Geolocation.watchPosition(
  //         (position) => this.setState({ location: position }),
  //         (error) => console.log(error),
  //         { enableHighAccuracy: false, distanceFilter: 0, interval: 70000, fastestInterval: 2000 }
  //     ));
  // }

  // removeLocationUpdates = () => {
  //     if (this.watchId !== null) {
  //         Geolocation.clearWatch(this.watchId);
  //         this.setState({ updatesEnabled: false })
  //     }
  // }

  componentDidMount = async () => {
    let pedido = JSON.parse(await getItem('Pedido'));
    let _session_ = JSON.parse(await isSession());

    this.setState({pedido: {...pedido}, session: {..._session_}});
    // this.getLocationUpdates();
  };

  finalizar = async status => {
    this.setState({
      load: true,
    });

    let dados = {
      _id: this.state.pedido._id,
      _idEntregador: this.state.session._id,
      token: this.state.session.token,
    };
    let response = await finalizarEntrega(dados);
    if (response.data.success) {
      this.setState({
        load: false,
      });
      this.props.navigation.navigate('Main');
    } else {
      Alert.alert(
        ':(',
        response.data.validacao[0].msg,
        [{text: 'OK', onPress: () => this.closeModal()}],
        {cancelable: false},
      );
      this.setState({
        load: false,
      });
    }
  };

  render() {
    const {pedido, infoRota} = this.state; //loading, location, updatesEnabled,
    const location = {};

    return (
      <View style={styles.subContainerMap}>
        <View style={styles.containerMap}>
          {/* {this.state.location ? (<Directions location={location}  destination={pedido.endereco.location}/>)  : (<View></View>)} */}
        </View>

        {pedido && pedido.usuario && (
          <View style={styles.boxBottom}>
            <Detalhes {...pedido} infoRota={infoRota} />
            <Buttons
              {...this.state}
              finalizar={this.finalizar.bind(this)}
              getLocationUpdates={this.getLocationUpdates.bind(this)}
            />
          </View>
        )}
      </View>
    );
  }
}
