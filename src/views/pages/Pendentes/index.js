import React, {Component} from 'react';
import {
  Text,
  ScrollView,
  TouchableOpacity,
  View,
  Image,
  RefreshControl,
} from 'react-native';
import {logOut} from '../../../services/auth';

import {pendentes} from '../../../services/Requests';
import {isSession} from '../../../services/auth';
import {setItem} from '../../../models/AsyncStorageModel';

import styles from './styles';

export default class Pendentes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pedidos: false,
      refresh: false,
    };

    this.redirectProduto = this.redirectProduto.bind(this);
  }

  getPedidos = async () => {
    await this.setState({refresh: true});
    let _isSession_ = JSON.parse(await isSession());
    let json = {token: _isSession_.token, tipoUsuario: _isSession_.tipo};
    if (_isSession_.tipo === 2) {
      json._idEstabelecimento = _isSession_._idEstabelecimento;
    }
    const pedidos = await pendentes(json);

    if (!pedidos.data.errors) {
      this.setState({
        pedidos: pedidos.data,
      });
    } else {
      this.setState({
        pedidos: false,
      });
    }
    this.setState({
      refresh: false,
    });
  };

  _onRefresh = async () => await this.getPedidos();

  handleOnNavigateBack = async () => await this._onRefresh();

  componentDidMount = async () => await this._onRefresh();

  sair() {
    logOut();
    this.props.navigation.navigate('Login');
  }

  redirectProduto = async pedido => {
    await setItem('Pedido', JSON.stringify(pedido));
    this.props.navigation.navigate('Pedido', {
      onNavigateBack: this.handleOnNavigateBack,
    });
  };

  pedido = () => {
    if (this.state.pedidos) {
      const pedidos = this.state.pedidos.map(pedido => (
        <ScrollView key={pedido._id} contentContainerStyle={styles.container}>
          <TouchableOpacity
            onPress={e => this.redirectProduto(pedido)}
            style={styles.item}>
            <View style={styles.head}>
              <Text style={styles.idPedido}>#{pedido._id}</Text>
              <Text style={styles.totalProdutos}>
                {pedido.produtos.length}{' '}
                {pedido.produtos.length < 1 ? 'Produtos' : 'Produto'}
              </Text>
              {/* <Text style={styles.textItem}>{pedido.dataHoraBR}</Text> */}
            </View>

            <View style={styles.body}>
              {pedido.produtos.map(produto => (
                <View key={produto._id} style={styles.estabelecimento}>
                  {console.log(
                    `https://deliveri.com.br/uploads/${
                      produto.estabelecimento.logo
                    }`,
                  )}

                  <Image
                    style={styles.logo}
                    source={{
                      uri: `https://deliveri.com.br/static/uploads/${
                        produto.estabelecimento.logo
                      }`,
                    }}
                  />
                  <Text style={styles.textItem}>
                    {produto.estabelecimento.nome}
                  </Text>
                </View>
              ))}

              <View style={styles.dataHora}>
                <Text style={styles.textItem}>{pedido.dataHoraBR}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      ));

      return pedidos;
    } else {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.nenhumPedido}>
            <Text style={styles.textCenter}>Nenhum pedido!</Text>
          </View>
        </ScrollView>
      );
    }
  };

  render() {
    return (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refresh}
            onRefresh={this._onRefresh}
          />
        }>
        {this.pedido()}
      </ScrollView>
    );
  }
}
