import {AsyncStorage} from 'react-native';

export const getItem = async item => AsyncStorage.getItem(item);
export const setItem = async (item, object) =>
  AsyncStorage.setItem(item, object);
export const removeItem = async item => AsyncStorage.removeItem(item);
