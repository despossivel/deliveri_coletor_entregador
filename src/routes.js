import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import Login from './views/pages/Login';
import Header from './views/globals/Header';
const Routes = createAppContainer(
  createSwitchNavigator({
    Login,
    Header,
  }),
);

export default Routes;
