import {AsyncStorage} from 'react-native';

export const TOKEN_KEY = '@blablablablablablablablabla';
export const isSession = async () => AsyncStorage.getItem(TOKEN_KEY);
export const session = async token =>
  AsyncStorage.setItem(TOKEN_KEY, JSON.stringify(token));
export const logOut = async () => AsyncStorage.removeItem(TOKEN_KEY);

//
// export const setPedidoEmCurso = async (dados) => AsyncStorage.setItem('pedidoEmCurso',JSON.stringify(dados));
// export const getPedidoEmCurso = async () => AsyncStorage.getItem('pedidoEmCurso');
