import axios from 'axios';
// import {IP, PORT} from './IP';

export const api = axios.create({
  // baseURL: `http://${IP}:${PORT}/api`
  baseURL: 'https://api.deliveri.com.br/api',
});

export default api;
