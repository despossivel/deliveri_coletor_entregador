import {api} from './api';

export const signIn = dados => api.post('/coletor/login', dados);
export const pendentes = dados => api.post('/coletor/pedidos/pendentes', dados);
export const aguardandoEntrega = dados =>
  api.post('/coletor/pedidos/prontos', dados);
export const agendados = dados => api.post('/coletor/pedidos/agendados', dados);
export const iniciarColeta = dados =>
  api.post('/coletor/pedido/coletar', dados);
export const finalizarColeta = dados =>
  api.post('/coletor/pedido/coletado', dados);
export const assumirEntrega = dados =>
  api.post('/entregador/assumir/entrega', dados);
export const finalizarEntrega = dados =>
  api.post('/entregador/finalizar/entrega', dados);
